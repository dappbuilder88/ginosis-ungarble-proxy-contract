const {expect}=require('chai');
const { ethers, upgrades } = require('hardhat');
let ERCV1;
let ercV1;
let ERCV2;
let ercV2;

describe("ERCV2 proxy functionality testing",function(){
    beforeEach(async function(){
        ERCV1=await ethers.getContractFactory("ERCVersion1");
        ERCV2=await ethers.getContractFactory("ERCVersion2");

        ercV1 =await upgrades.deployProxy(ERCV1,[10000],{initializer:"start"});
        ercV2 =await upgrades.upgradeProxy(ercV1.address,ERCV2);
    });
    it('Burning tokens in ERCV2 implementation',async function(){
        const amountToBeBurnt = 2000;
        await ercV2.burnTokens(amountToBeBurnt);
        const totalSupply = await ercV2.totalSupply();
        expect(totalSupply).to.equal(8000);
    });
});