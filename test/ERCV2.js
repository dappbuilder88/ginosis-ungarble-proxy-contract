const {expect}=require('chai');
const { ethers } = require('hardhat');

let ERCV2;
let ercV2;
let accounts;
let msgSender;

describe("ERCV2 test",function(){
    beforeEach(async function(){
        accounts = await ethers.getSigners();
        msgSender = accounts[0].address;
        ERCV2=await ethers.getContractFactory("ERCVersion2");
        ercV2 =await ERCV2.deploy();
        await ercV2.deployed();
        console.log(`ERCV2 deployed at ${ercV2.address}`);
    });

    it('Mint and burn test',async() => {
        const amountToBeMinted = 10000;
        const amountToBeBurnt = 2000;
        await ercV2.start(amountToBeMinted);
        await ercV2.burnTokens(amountToBeBurnt);
        const totalSupply = await ercV2.totalSupply();
        expect(totalSupply).to.equal(amountToBeMinted - amountToBeBurnt);
    });
    it("Transfer test", async()=> {
        const amountToBeMinted = 10000;
        await ercV2.start(amountToBeMinted);
        await ercV2.transfer(accounts[1].address, 2000);
        const tranfereeBalance = await ercV2.balanceOf(accounts[1].address);
        expect(tranfereeBalance).to.equal(2000);
    });
    it("Approve allowance", async()=> {
        const spender = accounts[1].address;
        await ercV2.approve(spender, 8000);
        const allowedAmount = await ercV2.allowance(msgSender, accounts[1].address);
        expect(allowedAmount).to.equal(8000);
    });
    it("Increased allowance", async()=> {
        const spender = accounts[1].address;
        await ercV2.approve(spender, 8000);
        await ercV2.increaseAllowance(spender, 5000);
        const allowedAmount = await ercV2.allowance(msgSender, accounts[1].address);
        expect(allowedAmount).to.equal(13000);
    });
    it("Decreased allowance", async()=> {
        const spender = accounts[1].address;
        await ercV2.approve(spender, 8000);
        await ercV2.decreaseAllowance(spender, 5000);
        const allowedAmount = await ercV2.allowance(msgSender, accounts[1].address);
        expect(allowedAmount).to.equal(3000);
    });
    it("Transfer from test", async()=> {
        const amountToBeMinted = 10000;
        let wallet = await ercV2.connect(accounts[0]);
        await wallet.start(amountToBeMinted);
        const spender = accounts[1].address;
        await ercV2.approve(spender, 8000);
        wallet = ercV2.connect(accounts[1]);
        await wallet.transferFrom(msgSender, accounts[2].address, 8000);
        const transfereeBalance = await ercV2.balanceOf(accounts[2].address);
        expect(transfereeBalance).to.equal(8000);
    });
});