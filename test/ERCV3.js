const {expect}=require('chai');
const { ethers } = require('hardhat');

let ERCV3;
let ercV3;
let accounts;
let msgSender;

describe("ERCV3 test",function(){
    beforeEach(async function(){
        accounts = await ethers.getSigners();
        msgSender = accounts[0].address;
        ERCV3=await ethers.getContractFactory("ERCVersion3");
        ercV3 =await ERCV3.deploy();
        await ercV3.deployed();
        console.log(`ERCV3 deployed at ${ercV3.address}`);
    });

    it("Only owner can burn tokens", async() => {
        const wallet = ercV3.connect(accounts[2]);
        await wallet.start(39000);
        await wallet.burnTokens(9000);
        expect(await ercV3.totalSupply()).to.equal(30000);
    });
    it("Only owner can transfer", async() => {
        const wallet = ercV3.connect(accounts[2]);
        await wallet.start(39000);
        await wallet.transfer(accounts[3].address, 7000);
        expect(await ercV3.balanceOf(accounts[2].address)).to.equal(32000);
    });
});