const {expect}=require('chai');
const { ethers, upgrades } = require('hardhat');
let ERCV1;
let ercV1;

describe("ERCV1 proxy testing",function(){
    beforeEach(async function(){
        ERCV1=await ethers.getContractFactory("ERCVersion1");
        ercV1 =await upgrades.deployProxy(ERCV1,[30000],{initializer:"start"});
    });

    it('Deploy',async function(){
        const totalSupply = await ercV1.totalSupply();
        expect(totalSupply).to.equal(30000);
    });
});