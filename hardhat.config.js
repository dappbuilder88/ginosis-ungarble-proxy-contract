require("@nomicfoundation/hardhat-toolbox");
require("@nomiclabs/hardhat-etherscan");
require("@openzeppelin/hardhat-upgrades");
require("dotenv").config();
module.exports = {
  solidity: "0.8.6",
  settings: {
    optimizer: {
      enabled: true,
      runs: 200,
    },
  },
  networks:{
    hardhat:{
      chainId:1337,
    },
    goerli:{
      url:process.env.ALCHEMY_API_KEY_GOERLI,
      accounts:[process.env.DEPLOYER_WALLET_PRIVATE_KEY],
      gas: 10000000
    }
  },
  etherscan:{
    apiKey: process.env.ETHERSCAN_API_KEY    
},
paths: {
  sources: "./contracts",
  tests: "./test",
  cache: "./cache",
  artifacts: "./artifacts"
}
};
