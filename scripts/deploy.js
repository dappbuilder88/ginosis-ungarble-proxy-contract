// scripts/deploy.js
async function main() {
    const ERCV1 = await ethers.getContractFactory("ERCVersion1");
    console.log("Deploying ERCV1...");
    const ercV1 = await upgrades.deployProxy(ERCV1, [1000000], { initializer: 'start' });
    console.log("ERCV1 deployed to:", ercV1.address);
  }
  
  main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });