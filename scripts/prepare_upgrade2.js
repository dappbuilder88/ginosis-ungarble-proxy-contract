async function main() {
    const proxyAddress = '0x917C610a37e34610be22e56cC8f8496b24CEF167';    // proxy address not the admin of the proxy.  
    const ERCV3 = await ethers.getContractFactory("ERCVersion3");
    console.log("Preparing upgrade...");
    const ERCV3Address = await upgrades.prepareUpgrade(proxyAddress, ERCV3);
    console.log("ERCV3 deployed at:", ERCV3Address);
  }
   
  main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });