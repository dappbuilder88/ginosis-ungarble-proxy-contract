const { ethers } = require('hardhat');
const { expect } = require('chai');

describe("ERCV3 proxy testing", async() => {
    let ERCV1, ercV1, ERCV3, ercV3, ERCV2, ercV2;
    it("Test", async() => {
        ERCV1 =  await ethers.getContractFactory("ERCVersion1");
        ERCV2 = await ethers.getContractFactory("ERCVersion2");
        ERCV3 = await ethers.getContractFactory("ERCVersion3");
        ercV1 = await upgrades.deployProxy(ERCV1, [20000], {initializer: "start"});
        await ercV1.deployed();
        ercV2 = await upgrades.upgradeProxy(ercV1.address, ERCV2);
        await ercV2.deployed();
        const amountToBeBurnt = 2000;
        await ercV2.burnTokens(amountToBeBurnt);
        const totalSupply = await ercV2.totalSupply();
        expect(totalSupply).to.equal(18000);
        ercV3 = await upgrades.upgradeProxy(ercV1.address, ERCV3);
        await ercV3.deployed();
        await ercV3.burnTokens(3000);
        const totalSupplyERCV3 = await ercV3.totalSupply();
        const totalSupplyERCV1 = await ercV1.totalSupply();
        expect(totalSupplyERCV3).to.equal(15000);
        expect(totalSupplyERCV3).to.equal(totalSupplyERCV1);
        console.log("ERCV3 proxy contract deploy on: ", ercV3.address);
    });
});