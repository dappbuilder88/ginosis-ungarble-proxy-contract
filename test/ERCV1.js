const {expect}=require('chai');
const { ethers } = require('hardhat');

let ERCV1;
let ercV1;

describe("ERCV1 testing",function(){
    before(async function(){
        ERCV1 = await ethers.getContractFactory("ERCVersion1");
        ercV1 = await ERCV1.deploy();
        await ercV1.deployed();
        console.log(`ERCV1 deploy at ${ercV1.address}`);
    });

    it('Mint test',async() => {
        const amountToBeMinted = 10000;
        await ercV1.start(amountToBeMinted);
        const totalSupply = await ercV1.totalSupply();
        expect(totalSupply).to.equal(amountToBeMinted);
    });
});