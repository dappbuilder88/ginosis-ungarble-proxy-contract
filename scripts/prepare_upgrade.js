async function main() {
    const proxyAddress = '0x917C610a37e34610be22e56cC8f8496b24CEF167';    // proxy address not the admin of the proxy.  
    const ERCV2 = await ethers.getContractFactory("ERCVersion2");
    console.log("Preparing upgrade...");
    const ERCV2Address = await upgrades.prepareUpgrade(proxyAddress, ERCV2);
    console.log("ERCV2 deployed at:", ERCV2Address);
  }
   
  main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });